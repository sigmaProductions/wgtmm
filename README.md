### _**Greetings!**_
First, thank you so much for your support! This was a labour of love, which i hope it may provide you and your table hours of enjoyment, filled with fun and convenience. 
If you have any problems with this Module then please let us know! Feel free to come join us over on our discord with any questions, doubts, or general feedback you might have.
>Share the love and let other people know about the module when you can! It means a lot.                                                      
> I wish you to Live, Love and Learn truly and fully but especially...                                 
> May them 20's be with your players! Even the right 1's too!
_- Sincerely, sigma._

## **PLEASE READ THESE STEPS WHEN IN DOUBT HOW TO INSTALL THE MODULE OR IMPORT ITS CONTENT**
- [ ] 1. When you first install the Module, and then enable it in your World, you will need to enable Scene Packer as well.
- [ ] 2. You will be asked at this point if you wish to **Import All** content or _Select Scenes_, it is recommended to use the Import All option.
- [ ] 3. It will take a minute or so, depending on your Machine, please wait until the process is finished and a notification is shown stating as much.
- [ ] 4. While the module imports the content in the background, you will be shown the Instructions Journal Entry.
- [ ] 5. Once all the content is imported, you can easily delete parts of it by right clicking a folder, and selecting the Delete All option. You can confidently delete content from your World at your leisure and re-import it at any time from the module's Compendia.
- [ ] 6. If you wish to re-import a Scene, please always use the menu talked about in 2. above, there is a macro in the Scene Packer module macro compendium which will let you reset said prompt aptly called "Reset world Scene Packer prompts". All you need to do is execute this macro, you can even do so from the compendium, and then refresh the page (you can either press F5 for most browsers or reconnect to the World by returning to Foundry's setup screen and then launching the world again)



## **Foundry Module Synergies:**

Some optional features can be taken advantaged of if you use any of the following modules:
_(If you do not use any of these modules it should not cause any issue or deprive you of any fundamental functionality to run this Module's Content.
We highly recommend the use of these modules to enhance your table's playing experience.)_



1.  Monk's Active Tile Triggers: Used for teleporting tokens. Either by entering a demarked tile, for example , or double-clicking the . Also used to Activate Traps or other special mechanisms.

2. Smalltime: Used to control Scene Lightining in accordance to in-game time of day.

3. Perfect Vision: Used to replicate a much more faithful Vision according to the D&D 5th Edition rules for Darkvision or lack thereof. It also provides a solution for Scenes with Daylight ilumination outside but dark interiors.

4. Lock View: Helps to set how you want the Scene to be seen by your players upon their first viewing in terms of coordinates and zoom level.

5. Midi QoL and Dynamic Active Effects (aka DAE): Some Active Effects with Midi specific Flags were included, as well as midi specific data on some items.

6. Compendium Folders: Organisation makes life so much better. And folders make organising content great.

7. Polyglot: Coverts text in a given in-game language to a set of runes that only those who's associated Character that knows the language can read (or a GM).

8. Automated Evocations, which uses The Sequencer and Warpgate: With these, you can spawn tokens like companions or other tokens related to any given Character directly from the Character Sheet. (Ex: Jaziel can be Evoked from Azân, a Phantasmal version of the Chimera can be Evoked by it's respective ability.)

9. If you are using Foundry v9 or above, Monk's TokenBar and Tagger: These allow extra functionality to be used with Monk's Active Tile Triggers.

10. Merchant Sheet NPC, Loot Sheet NPC 5e: These modules provide an alternative Character Sheet to the default NPC sheet that allows players to interact directly with merchants, observing the stock and buy and or sell items, including price modifiers. (They come with the default modifiers which you can adjust on the fly in response to Barganing the players may attempt)
